let dniNumber = prompt("Cual es el nuemro de tu DNI?")
let dniLetter = prompt("Cual es la letra de tu DNI") 

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
							'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

function dniChecker() {
    let numberRemainder = dniNumber % 23;
    let remainderLetter = letras[numberRemainder];
    if (dniNumber < 0 || dniNumber > 99999999) {
      console.log("Numero Invalido");
    }
    else if (dniLetter === remainderLetter) {
      console.log("Numero de DNI correcto!")
    }
    else {
      console.log("Numero de DNI incorrecto.")
    }
}

dniChecker();